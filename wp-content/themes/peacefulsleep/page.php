<?php get_header(); the_post(); ?>
    <!-- Page header
    ================================================== -->
    <header class="header">
        <div class="header__background" style="background-image: url(<?php echo the_post_thumbnail_url('img_2000x1333') ?>); background-position: bottom center"></div>
        <div class="header__overlay"></div>
        <h1 class="header__title text-white"><?php the_title() ?></h1>
    </header>


    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Content
    ================================================== -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <?php the_content() ?>
                    <?php edit_post_link( __( 'Edit', 'base' ) ); ?>
                    <?php wp_link_pages(); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>