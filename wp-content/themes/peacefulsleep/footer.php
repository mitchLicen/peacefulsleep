<?php
$f_logo = get_field('f_logo', 'option');
$f_logo_link = get_field('f_logo_link', 'option');
$f_menu_1_title = get_field('f_menu_1_title', 'option');
$f_menu_1 = get_field('f_menu_1', 'option');
$f_menu_2_title = get_field('f_menu_2_title', 'option');
$f_menu_2 = get_field('f_menu_2', 'option');
//$f_youtube_block_title = get_field('f_youtube_block_title', 'option');
$f_menu_3_title = get_field('f_menu_3_title', 'option');
$f_menu_3 = get_field('f_menu_3', 'option');
$f_youtube_show = get_field('f_show_youtube_icon', 'option');
$f_youtube_link = get_field('f_youtube_link', 'option');
//$f_youtube_additional_link_title = get_field('f_youtube_additional_link_title', 'option');
//$f_youtube_additional_link = get_field('f_youtube_additional_link', 'option');
$f_social_share_title = get_field('f_social_share_title', 'option');
$f_footer_text = get_field('f_footer_text', 'option');
?>
    <footer>
        <div class="container">
            <?php if($f_logo): ?>
            <a href="<?php echo $f_logo_link; ?>" target="_blank"><div class="tb-logo"><?php echo wp_get_attachment_image($f_logo['id'], 'full'); ?></div></a>
            <?php endif; ?>
            <div class="row footer__top">
                <?php if( $f_menu_1 ): ?>
                <!-- Link section-->
                <div class="col-xs-6 col-sm-2 footer__link-section">
                    <?php if($f_menu_1_title): ?><h5 class="text-blue text-uppercase"><?php echo $f_menu_1_title; ?></h5><?php endif; ?>
                    <?php
                    wp_nav_menu( array(
                            'menu' => $f_menu_1,
                            'container' => false,
                            'menu_class' => 'noclass'
                        )
                    );
                    ?>
                </div>
                <?php endif; ?>
                <?php if( $f_menu_2 ): ?>
                    <!-- Link section-->
                    <div class="col-xs-6 col-sm-2 footer__link-section">
                        <?php if($f_menu_2_title): ?><h5 class="text-blue text-uppercase"><?php echo $f_menu_2_title; ?></h5><?php endif; ?>
                        <?php
                        wp_nav_menu( array(
                                'menu' => $f_menu_2,
                                'container' => false,
                                'menu_class' => 'noclass'
                            )
                        );
                        ?>
                    </div>
                <?php endif; ?>
                <?php if($f_menu_3_title): ?>
									<!-- Link section-->
									<div class="col-xs-6 col-sm-2 footer__link-section">
											<h5 class="text-blue text-uppercase"><?php echo $f_menu_3_title; ?></h5>
													<ul>
															<?php if($f_youtube_show): ?>
																	<li><a href="<?php echo $f_youtube_link; ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
															<?php endif; ?>
															<?php if($f_menu_3): ?>
																	<li>
																		<?php
																		wp_nav_menu( array(
																						'menu' => $f_menu_3,
																						'container' => false,
																						'menu_class' => 'noclass'
																				)
																		);
																		?>
																	</li>
															<?php endif; ?>
													</ul>
									</div>
                <?php endif; ?>
                <!-- Link section-->
                <div class="footer__link-section footer__social">
                    <?php if($f_social_share_title): ?><h5><?php echo $f_social_share_title; ?></h5><?php endif; ?>
                    <div class="sharethis-inline-share-buttons" data-url="<?php echo home_url()?>" data-title="<?php echo get_bloginfo('name'); ?>"></div>
                </div>
            </div>
            <?php if($f_footer_text): ?>
            <div class="row footer__bottom">
                <div class="col-sm-12 tiny-text"><?php echo str_replace(array('[year]'), array(date_i18n('Y')), $f_footer_text); ?></div>
            </div>
            <?php endif; ?>
        </div>
    </footer>
    <?php if(is_single()): ?>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=599c568b6d61950012975455&product=sticky-share-buttons' async='async'></script>
    <?php else: ?>
        <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=599c53dc6d61950012975451&product=inline-share-buttons' async='async'></script>
    <?php endif; ?>
	<?php wp_footer(); ?>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NL55PQP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>