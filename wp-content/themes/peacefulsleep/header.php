<!--

	"Wherever you go and whatever you do, you will be blessed." 
	~ Deuteronomy 28:6
	
	"Thank you Lord for giving us the opportunity to have worked with this client. 
	Thank you for enabling us to do our job properly and deliver this project. 
	We pray that you bless this website and business with your endless love and grace. 
	Amen!" 
	
-->

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="name" content="<?php bloginfo('name') ?>"/>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="<?php bloginfo('name') ?>">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name') ?>">
    <meta name="theme-color" content="#ffffff">
    <meta name="robots" content="index, follow">


    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.svg">
    
    <?php wp_head(); ?>

<meta name="google-site-verification" content="IiwwGpsMcv_39gAr2aKlN71L0R3kj7wo0fRnMf89RpQ" />
    
   <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NL55PQP');</script>
<!-- End Google Tag Manager -->

<!--Content Central Tag-->
<meta name="google-site-verification" content="IiwwGpsMcv_39gAr2aKlN71L0R3kj7wo0fRnMf89RpQ" />


   <!--Old Tag -- <?php the_field('h_ga_snippet', 'option') ?>--->
    
</head>
<body <?php body_class(); ?>>
  <?php
  $q_start_image = get_field('q_start_image', $hid);
  ?>
  	<!-- Mobile modal
    ================================================== -->
    <div id="mobFirstModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="col-sm-12">
            <div class="col-sm-4 text-center __image">
              <?php echo wp_get_attachment_image($q_start_image['id'], 'full'); ?>
            </div>

            <div class="col-sm-8 text-center">
              <h1 class="text-blue">Find out which mosquito repellent is best for me?</h1>
              <div class="vert-spacer-10"></div>
              <button type="button" class="btn btn-blue" data-label="yesbtn">Yes</button>
              <button type="button" class="btn btn-ghost" data-label="nobtn">Not now</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Menu
    ================================================== -->
    <!--Main Navigation-->
    <div class="col-sm-12 menu">
        <div class="row">
            <div class="container">

                <nav class="navbar navbar-default">
                    <!-- Header -->
                    <div class="navbar-header">
                        <!--Mobile burger icon -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Logo -->
                        <?php if($logo_url = get_custom_logo_url()): ?>
                        <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php echo $logo_url; ?>" alt="<?php bloginfo('name') ?>" height="100%"></a>
                        <?php endif; ?>
                    </div>

                    <!-- Menu items / Mobile dropdown -->
                    <div class="collapse navbar-collapse" id="main-nav">
                        <?php get_search_form(); ?>

                        <?php
                        if( has_nav_menu( 'nav' ) )
                            wp_nav_menu( array(
                                    'container' => false,
                                    'theme_location' => 'nav',
                                    'menu_class' => 'nav navbar-nav navbar-right'
                                )
                            );
                        ?>
                    </div>
                </nav>

            </div>
        </div>
    </div>


    <!-- Hero slider
    ================================================== -->
    <?php if( have_rows('slider') ): ?>
    <section class="hero-slider">
        <div class="owl-carousel" id="hero-slider">
            <?php while ( have_rows('slider') ) : the_row();
                $overlay_color = get_sub_field('overlay_color');
                $overlay_opacity = get_sub_field('overlay_opacity');
                $title_color = get_sub_field('title_color');
                $text_color = get_sub_field('text_color');
                $button_color = get_sub_field('button_color');
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $button_link = get_sub_field('button_link');
                $button_title = get_sub_field('button_title'); ?>
                <?php if($image): ?>
                    <div class="hero-slider__slide">
                        <div class="hero-slider__background" style="background-image: url(<?php echo wp_get_attachment_image_url($image['id'], 'img_2000x1333'); ?>"></div>
                        <div class="hero-slider__mossies"></div>
                        <div class="hero-slider__mask-image" style="background-image: url(<?php echo wp_get_attachment_image_url($image['id'], 'img_2000x1333'); ?>"></div>
                        <div class="hero-slider__overlay"<?php if($overlay_color): ?> style="background:<?php echo hexToRgb($overlay_color, $overlay_opacity); ?>;"<?php endif; ?>></div>
                        <div class="hero-slider__frame"></div>
                        <div class="hero-slider__content">
                            <?php if($title): ?><h1 class="hero-slider__title text-<?php echo $title_color; ?>"><?php echo $title; ?></h1><?php endif; ?>
                            <?php if($text): ?><p class="hero-slider__subtitle text-<?php echo $text_color; ?>"><?php echo $text; ?></p><?php endif; ?>
                            <?php if($button_link): ?>
                            <a href="<?php echo $button_link; ?>"><button type="button" class="btn btn-<?php echo $button_color; ?>"><?php echo $button_title; ?></button></a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
        <div class="pagination-dots" id="hero-dots"></div>
        <div class="nav-arrows__right"><i class="material-icons">chevron_right</i></div>
        <div class="nav-arrows__left"><i class="material-icons">chevron_left</i></div>
    </section>
    <?php endif; ?>