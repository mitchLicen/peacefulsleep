<?php
class Custom_Widget_Categories extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'peacefulsleep_widget_categories',
            'description' => __( 'A list of categories.' ),
            'customize_selective_refresh' => true,
        );
        parent::__construct( 'peacefulsleep_categories', __( 'PeacefulSleep Categories' ), $widget_ops );
    }

    public function widget( $args, $instance ) {
        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        $c = ! empty( $instance['count'] ) ? '1' : '0';
        $h = ! empty( $instance['hierarchical'] ) ? '1' : '0';
        $taxonomy = $instance['taxonomy'] ? $instance['taxonomy'] : 'category';

        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        $cat_args = array(
            'orderby'      => 'name',
            'show_count'   => $c,
            'hierarchical' => $h,
            'taxonomy'     => $taxonomy,
            'walker'       => new Custom_Widget_Walker_Category
        );
        ?>
        <ul class="list-group">
            <?php
            $cat_args['title_li'] = '';
            wp_list_categories( apply_filters( 'widget_categories_args', $cat_args ) );
            ?>
        </ul>
        <?php

        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );
        $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
        $instance['taxonomy'] = sanitize_text_field( $new_instance['taxonomy'] );

        return $instance;
    }

    public function form( $instance ) {
        //Defaults
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = sanitize_text_field( $instance['title'] );
        $count = isset($instance['count']) ? (bool) $instance['count'] :false;
        $taxonomy = sanitize_text_field( $instance['taxonomy'] );

        $taxonomies = get_taxonomies(array(), 'objects');
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

        <?php if ( ! empty( $taxonomies ) ) : ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'taxonomy' ); ?>"><?php _e( 'Select Taxonomy:' ); ?></label>
            <select id="<?php echo $this->get_field_id( 'taxonomy' ); ?>" name="<?php echo $this->get_field_name( 'taxonomy' ); ?>">
                <option value="0"><?php _e( '&mdash; Select &mdash;' ); ?></option>
                <?php foreach ( $taxonomies as $tax ) : ?>
                    <option value="<?php echo esc_attr( $tax->name ); ?>" <?php selected( $taxonomy, $tax->name ); ?>>
                        <?php echo esc_html( $tax->label ); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
        <?php endif; ?>

        <p>
            <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label></p>
        <?php
    }

}

add_action( 'widgets_init', create_function( '', 'return register_widget( "Custom_Widget_Categories" );' ) );


class Custom_Widget_Post_Featured_Product extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'peacefulsleep_post_featured_product',
            'description' => __( 'Get featured product from post custom field.' ),
        );
        parent::__construct( 'peacefulsleep_post_featured_product', __( 'PeacefulSleep Post Featured Product' ), $widget_ops );
    }

    public function widget( $args, $instance ) {
        /** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
        $fp_ids_1 = get_field('featured_product');
        $fp_ids_2 = get_field('featured_products');
				$fp_ids = [];
			
				if(!empty($fp_ids_2)){
					$fp_ids = array_merge($fp_ids_1, $fp_ids_2);
				}else{
					$fp_ids = $fp_ids_1;
				}
        
				//print_r($fp_ids);
			
        if(!empty($fp_ids)) {
            echo $args['before_widget'];
            if ($title) {
                echo $args['before_title'] . $title . $args['after_title'];
            }
            ?>
            <div class="owl-carousel" id="featured-products">
                <?php foreach ($fp_ids as $fp_id) :
                    $product_category = get_the_term_list($fp_id, 'product_category', '', ', '); ?>
                    <div class="card product-related">
                        <div class="card__container">
                            <?php if(has_post_thumbnail($fp_id)): ?>
                                <div class="card__image">
                                    <?php echo get_the_post_thumbnail($fp_id, 'full'); ?>
                                </div>
                            <?php endif; ?>
                            <div class="card__heading"><a href="<?php echo get_permalink($fp_id); ?>"><h4 class="text-blue"><?php echo get_the_title($fp_id) ?></h4></a></div>
                            <?php if($product_category): ?><div class="card__tag"><?php echo $product_category; ?></div><?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="pagination-dots" id="featured-products-dots"></div>
            <?php
            echo $args['after_widget'];
        }
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field( $new_instance['title'] );

        return $instance;
    }

    public function form( $instance ) {
        //Defaults
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = sanitize_text_field( $instance['title'] );
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>
        <?php
    }

}

add_action( 'widgets_init', create_function( '', 'return register_widget( "Custom_Widget_Post_Featured_Product" );' ) );
