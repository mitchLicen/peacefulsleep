<?php

class wpse_99513_adjacent_category {

    public $sorted_taxonomies,$taxonomy,$loop_links;

    /**
     * @param string Taxonomy name. Defaults to 'category'.
     * @param string Sort key. Defaults to 'id'.
     * @param boolean Whether to show empty (no posts) taxonomies.
     * @param boolean Show first category as
     */
    public function __construct( $taxonomy = 'category', $order_by = 'id', $skip_empty = true, $loop_links = true ) {
        $this->taxonomy = $taxonomy;
        $this->loop_links = $loop_links;

        $this->sorted_taxonomies = get_terms(
            $this->taxonomy,
            array(
                'get'          => $skip_empty ? '' : 'all',
                'fields'       => 'ids',
                'hierarchical' => false,
                'order'        => 'ASC',
                'orderby'      => $order_by,
            )
        );
    }

    /**
     * @param int Taxonomy ID.
     * @return int|bool Next taxonomy ID or false if this ID is last one. False if this ID is not in the list.
     */
    public function next( $taxonomy_id ) {

        $current_index = array_search( $taxonomy_id, $this->sorted_taxonomies );

        if ( false !== $current_index ){
            if(isset( $this->sorted_taxonomies[ $current_index + 1 ] ) ){
                $search_id = $this->sorted_taxonomies[$current_index + 1];
            }elseif(count($this->sorted_taxonomies) > 2 && $this->loop_links) {
                $search_id = array_shift($this->sorted_taxonomies);
            }
            return get_term($search_id, $this->taxonomy);
        }

        return false;
    }

    /**
     * @param int Taxonomy ID.
     * @return int|bool Previous taxonomy ID or false if this ID is last one. False if this ID is not in the list.
     */
    public function previous( $taxonomy_id ) {

        $current_index = array_search( $taxonomy_id, $this->sorted_taxonomies );

        if ( false !== $current_index ) {
            if (isset($this->sorted_taxonomies[$current_index - 1])){
                $search_id = $this->sorted_taxonomies[$current_index - 1];
            }elseif(count($this->sorted_taxonomies) > 2 && $this->loop_links) {
                $search_id = array_pop($this->sorted_taxonomies);
            }
            return get_term($search_id, $this->taxonomy);
        }

        return false;
    }
}


class Custom_Widget_Walker_Category extends Walker {
    public $tree_type = 'category';

    public $db_fields = array ('parent' => 'parent', 'id' => 'term_id');

    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        if ( 'list' != $args['style'] )
            return;

        $indent = str_repeat("\t", $depth);
        $output .= "$indent<ul class='children'>\n";
    }

    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( 'list' != $args['style'] )
            return;

        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        /** This filter is documented in wp-includes/category-template.php */
        $cat_name = apply_filters(
            'list_cats',
            esc_attr( $category->name ),
            $category
        );

        // Don't generate an element if the category name is empty.
        if ( ! $cat_name ) {
            return;
        }

        $link = '<a href="' . esc_url( get_term_link( $category ) ) . '" ';
        if ( $args['use_desc_for_title'] && ! empty( $category->description ) ) {
            $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
        }

        $link .= '>';
        $link .= $cat_name . '</a>';

        if ( ! empty( $args['feed_image'] ) || ! empty( $args['feed'] ) ) {
            $link .= ' ';

            if ( empty( $args['feed_image'] ) ) {
                $link .= '(';
            }

            $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $args['feed_type'] ) ) . '"';

            if ( empty( $args['feed'] ) ) {
                $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
            } else {
                $alt = ' alt="' . $args['feed'] . '"';
                $name = $args['feed'];
                $link .= empty( $args['title'] ) ? '' : $args['title'];
            }

            $link .= '>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= $name;
            } else {
                $link .= "<img src='" . $args['feed_image'] . "'$alt" . ' />';
            }
            $link .= '</a>';

            if ( empty( $args['feed_image'] ) ) {
                $link .= ')';
            }
        }

        if ( ! empty( $args['show_count'] ) ) {
            $link .= ' <span class="badge">' . number_format_i18n( $category->count ) . '</span>';
        }
        if ( 'list' == $args['style'] ) {
            $output .= "\t<li";
            $css_classes = array(
                'list-group-item',
                'cat-item-' . $category->term_id,
            );

            if ( ! empty( $args['current_category'] ) ) {
                // 'current_category' can be an array, so we use `get_terms()`.
                $_current_terms = get_terms( $category->taxonomy, array(
                    'include' => $args['current_category'],
                    'hide_empty' => false,
                ) );

                foreach ( $_current_terms as $_current_term ) {
                    if ( $category->term_id == $_current_term->term_id ) {
                        $css_classes[] = 'current-cat';
                    } elseif ( $category->term_id == $_current_term->parent ) {
                        $css_classes[] = 'current-cat-parent';
                    }
                    while ( $_current_term->parent ) {
                        if ( $category->term_id == $_current_term->parent ) {
                            $css_classes[] =  'current-cat-ancestor';
                            break;
                        }
                        $_current_term = get_term( $_current_term->parent, $category->taxonomy );
                    }
                }
            }

            $css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );

            $output .=  ' class="' . $css_classes . '"';
            $output .= ">$link\n";
        } elseif ( isset( $args['separator'] ) ) {
            $output .= "\t$link" . $args['separator'] . "\n";
        } else {
            $output .= "\t$link<br />\n";
        }
    }

    public function end_el( &$output, $page, $depth = 0, $args = array() ) {
        if ( 'list' != $args['style'] )
            return;

        $output .= "</li>\n";
    }

}