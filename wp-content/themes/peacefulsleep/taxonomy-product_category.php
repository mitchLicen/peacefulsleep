<?php get_header();
$term = get_queried_object();
$page_image = get_field('page_image', $term);
$category_ids = new wpse_99513_adjacent_category( $term->taxonomy, 'id' );
$next_category = $category_ids->next( $term->term_id );
$previous_category = $category_ids->previous( $term->term_id );
?>

    <!-- Header
    ================================================== -->
    <header class="header">
        <?php if($page_image): ?><div class="header__background" style="background-image: url(<?php echo wp_get_attachment_image_url($page_image['id'], 'img_2000x1333'); ?>); background-position: top center;"></div><?php endif; ?>
        <div class="header__overlay"></div>
        <h1 class="header__title text-white"><?php single_cat_title(); ?></h1>
        <?php if($previous_category): ?><div class="nav-arrow nav-arrow__left" data-link="<?php echo get_term_link($previous_category); ?>"><i class="material-icons">chevron_left</i></div><?php endif; ?>
        <?php if($next_category): ?><div class="nav-arrow nav-arrow__right" data-link="<?php echo get_term_link($next_category); ?>"><i class="material-icons">chevron_right</i></div><?php endif; ?>
    </header>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>

    <!-- Content
    ================================================== -->
    <section class="product-list">
        <div class="container">
            <div class="row">

                <!-- Product list
                ================================================== -->
                <div class="col-sm-9 col-sm-push-3">
                    <div class="row">
                        <?php if ( have_posts() ) : ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php get_template_part( 'blocks/content', get_post_type() ); ?>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <p><?php _e('No products exist.', 'base'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <!-- Side widget
                ================================================== -->
                <div class="col-sm-3 col-sm-pull-9 widget-sticky">
                    <?php dynamic_sidebar('left_products') ?>
                </div>

            </div>

            <div class="vert-spacer-50"></div>

            <!-- Pager
            ================================================== -->
            <div class="col-sm-12 bottom-category-btns">
                <div class="row">
                    <?php if($previous_category): ?><a href="<?php echo get_term_link($previous_category); ?>" class="__left"><button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> <?php echo $previous_category->name; ?></button></a><?php endif; ?>
                    <?php if($next_category): ?><a href="<?php echo get_term_link($next_category); ?>" class="__right"><button type="button" class="btn btn-ghost"><?php echo $next_category->name; ?> <i class="material-icons">chevron_right</i></button></a><?php endif; ?>
                </div>
            </div>

        </div>
    </section>


    <?php get_template_part( 'blocks/which-product' ); ?>

    <?php get_template_part( 'blocks/where-to-buy'); ?>

<?php get_footer(); ?>