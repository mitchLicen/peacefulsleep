<?php get_header(); the_post();
$categories = get_the_term_list(get_the_ID(), 'category', '', ', ');
$subtitle = get_field('subtitle');
$featured_product = get_field('featured_product');
$prev_post = get_adjacent_post( true, '' );
if(!$prev_post){
    $prev_post = get_first_last_post( true );
}
if($prev_post) {
    $prev_posts_url = get_permalink($prev_post);
}
$next_post = get_adjacent_post( true, '', false );
if(!$next_post){
    $next_post = get_first_last_post(true, '', false);
}
if($next_post) {
    $next_posts_url = get_permalink($next_post);
}
?>
    <!-- Header
    ================================================== -->
    <header class="blog-header">
        <?php get_template_part( 'blocks/content-featured-single', get_post_type() ); ?>
        <?php if($prev_posts_url): ?><div class="nav-arrow nav-arrow__left" data-link="<?php echo $prev_posts_url; ?>"><i class="material-icons">chevron_left</i></div><?php endif; ?>
        <?php if($next_posts_url): ?><div class="nav-arrow nav-arrow__right" data-link="<?php echo $next_posts_url; ?>"><i class="material-icons">chevron_right</i></div><?php endif; ?>
    </header>


    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Post content
    ================================================== -->
    <section>
        <div class="container">
            <div class="row">

                <!-- Content -->
                <div class="col-sm-9 post-content">
                    <?php the_content() ?>
                    <?php wp_link_pages(); ?>
                </div>

                <!-- Side widget -->
                <aside class="col-sm-3 widget-sticky">
                    <?php dynamic_sidebar('single_post') ?>
                </aside>

            </div>
            <?php get_template_part( 'blocks/pager-single', get_post_type() ); ?>
        </div>
    </section>
<?php get_footer(); ?>