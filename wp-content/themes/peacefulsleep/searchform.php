<form method="get"class="navbar-form navbar-right" action="<?php echo home_url(); ?>" >
    <div class="form-group">
        <input type="text" class="form-control" placeholder="<?php _e( 'Search', 'base' ); ?>" name="s" value="<?php echo get_search_query(); ?>" >
        <i class="material-icons">search</i>
    </div>
</form>