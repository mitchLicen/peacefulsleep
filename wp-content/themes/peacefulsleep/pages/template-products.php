<?php
/*
Template Name: Products
*/
get_header(); the_post();
$page_title = get_field('page_title'); ?>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs hide-mobile">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>

    <!-- Product range
    ================================================== -->
    <?php $product_terms = get_terms('product_category');
    if($product_terms): ?>
        <header class="products-header">
            <div class="__background" style="background-image: url(<?php echo the_post_thumbnail_url('img_2000x1333') ?>); background-position: bottom center"></div>
            <div class="__overlay"></div>
                <div class="__content">
                    <div class="container">
                        <div class="row">

                        <?php if($page_title): ?><h1 class="__title text-white text-center"><?php echo $page_title; ?></h1><?php endif; ?>
                        <div class="owl-carousel" id="product-slider">
                            <?php foreach ($product_terms as $product_term): ?>
                                <?php include(locate_template('blocks/category-item.php')); ?>
                            <?php endforeach; ?>
                        </div>

                        <div class="pagination-dots" id="product-dots"></div>
                    </div>
                </div>
            </div>
        </header>
    <?php endif; ?>

    <?php get_template_part( 'blocks/which-product' ); ?>

    <?php get_template_part( 'blocks/where-to-buy'); ?>

<?php get_footer(); ?>