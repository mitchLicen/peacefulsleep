<?php
/*
Template Name: Blog
*/
get_header(); ?>

    <!-- Featured blog post
    ================================================== -->
    <?php
    $q = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 1,
    ));
    if ($q->have_posts()) : $q->the_post(); ?>
        <header class="blog-header">
            <?php get_template_part( 'blocks/content-featured', get_post_type() ); ?>
        </header>
    <?php wp_reset_query(); endif; ?>


    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Blog categories
    ================================================== -->
    <?php $categories = get_terms('category', array('hide_empty' => 0));
    if($categories): ?>
    <section class="">
        <div class="container">
            <div class="row">
                <?php foreach ($categories as $cat):
                    $image = get_field('image', $cat); ?>
                    <div class="col-sm-4 xs-gutter">
                        <?php if($cat->count > 0): ?><a href="<?php echo get_term_link($cat); ?>"><?php endif; ?>
                            <div class="category-card">
                                <div class="__background" style="background-image: url(<?php echo wp_get_attachment_image_url($image['id'], 'img_1500x'); ?>)"></div>
                                <div class="__overlay"></div>
                                <div class="__title"><h1 class="text-white"><?php echo $cat->name; ?></h1></div>
                                <span class="badge"><?php echo $cat->count; ?></span>
                            </div>
                        <?php if($cat->count > 0): ?></a><?php endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php endif; ?>
<?php get_footer(); ?>