<?php
/*
Template Name: Home
*/
get_header(); the_post(); ?>

    <?php get_template_part( 'blocks/which-product' ); ?>

    <!-- Product range
    ================================================== -->
    <?php $product_terms = get_terms('product_category');
    if($product_terms): ?>
        <section class="product-range">
            <div class="container">
                <div class="row">
                    <div class="owl-carousel" id="product-slider">
                        <?php foreach ($product_terms as $product_term): ?>
                            <?php include(locate_template('blocks/category-item.php')); ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="pagination-dots" id="product-dots"></div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php get_template_part( 'blocks/where-to-buy'); ?>

    <!-- Featured blog post
    ================================================== -->
    <?php
    $q = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 1,
    ));
    if ($q->have_posts()) : $q->the_post();
        $main_post_id = get_the_ID();
        $main_post_categories = get_the_terms( get_the_ID(), 'category' );
        $categories = get_the_term_list(get_the_ID(), 'category', '', ', ');
        $subtitle = get_field('subtitle');
        $featured_product = get_field('featured_product'); 
				$overlay_color = get_field('featured_overlay_color');
				$overlay_opacity = get_field('featured_overlay_opacity');
				$title_color = get_field('featured_title_color');
				$subtitle_color = get_field('featured_subtitle_color');
				$button_title = get_field('featured_button_title');
				$button_color = get_field('featured_button_color');
				$category_tag_theme = get_field('featured_category_tag_theme');
				?>
        <section class="featured-post">
            <div class="featured-post__container">
                <div class="featured-post__background" style="background-image: url(<?php the_post_thumbnail_url('img_2000x1333'); ?>)"></div>
                <div class="featured-post__mossies"></div>
                <div class="featured-post__mask-image" style="background-image: url(<?php the_post_thumbnail_url('img_2000x1333'); ?>)"></div>
                <div class="featured-post__overlay"<?php if($overlay_color): ?> style="background:<?php echo hexToRgb($overlay_color, $overlay_opacity); ?>;"<?php endif; ?>></div>
                <div class="featured-post__frame"></div>
                <div class="featured-post__content">
                    <div class="featured-post__category <?php echo $category_tag_theme; ?>"><?php echo $categories; ?></div>
                    <h1 class="featured-post__title <?php echo $title_color; ?>"><?php the_title() ?></h1>
                    <?php if($subtitle): ?><h4 class="featured-post__subtitle <?php echo $subtitle_color; ?>"><?php echo $subtitle; ?></h4><?php endif; ?>
                    <a href="<?php echo the_permalink(); ?>"><button type="button" class="btn <?php echo $button_color; ?>"><?php _e($button_title, 'base'); ?></button></a>
                </div>
            </div>
            <div class="featured-post__date">
                <span class="month"><?php echo the_time('M'); ?></span>
                <span class="date"><?php echo the_time('d'); ?></span>
                <span class="year"><?php echo the_time('Y'); ?></span>
            </div>
            <?php if($featured_product): ?>
                <div class="featured-post__feature-product">
                    <?php echo get_the_post_thumbnail($featured_product[0], 'full'); ?>
                </div>
            <?php endif; ?>
        </section>
    <?php wp_reset_query(); endif; ?>


    <!-- Related blog posts
    ================================================== -->
    <?php if($main_post_categories[0]): ?>
        <?php
        $q = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 3,
            //'cat' => $main_post_categories[0]->term_id,
            'post__not_in' => array($main_post_id)
        ));
        if ($q->have_posts() && $q->found_posts >= 3) : ?>
            <section class="related-post">
                <h6 class="text-uppercase text-grey text-center"><?php _e('other recent posts', 'base'); ?></h6>
                <div class="owl-carousel" id="related-posts-slider">
                    <?php while ($q->have_posts()) : $q->the_post();
                        $categories = get_the_term_list(get_the_ID(), 'category', '', ', '); 
												$overlay_color = get_field('featured_overlay_color');
												$overlay_opacity = get_field('featured_overlay_opacity');
												$title_color = get_field('featured_title_color');
												$button_title = get_field('featured_button_title');
												$button_color = get_field('featured_button_color');
												$category_tag_theme = get_field('featured_category_tag_theme');
										?>
                        <div class="related-post__post" style="background-image: url(<?php the_post_thumbnail_url('img_2000x1333'); ?>)">
                            <div class="related-post__overlay"></div>
                            <div class="related-post__content">
                                <div class="related-post__category dark"><?php echo $categories; ?></div>
                                <h1 class="related-post__title <?php echo $title_color; ?>"><?php the_title() ?></h1>
                                <a href="<?php the_permalink() ?>"><button type="button" class="btn <?php echo $button_color; ?>"><?php _e($button_title, 'base'); ?></button></a>
                            </div>
                            <div class="related-post__date">
                                <span class="month"><?php echo the_time('M'); ?></span>
                                <span class="date"><?php echo the_time('d'); ?></span>
                                <span class="year"><?php echo the_time('Y'); ?></span>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="pagination-dots" id="related-posts-dots"></div>
            </section>
        <?php wp_reset_query(); endif; ?>
    <?php endif; ?>

    <!-- Our story
    ================================================== -->
    <?php
    $os_title = get_field('os_title');
    $os_image = get_field('os_image');
    $os_text = get_field('os_text');
    $os_link = get_field('os_link');
    if($os_title || $os_image || $os_text || $os_link): ?>
    <section class="our-story no-vert-padding">
        <?php if($os_image): ?>
        <div class="our-story__background" style="background-image: url(<?php echo wp_get_attachment_image_url($os_image['id'], 'img_2000x1333'); ?>    )"></div>
        <?php endif; ?>
        <div class="our-story__overlay"></div>
        <div class="our-story__content col-xs-12 col-sm-8 col-sm-offset-2 col-md-5 col-md-offset-2">
            <?php if($os_title): ?><h1 class="our-story__title text-white"><?php echo $os_title; ?></h1><?php endif; ?>
            <?php if($os_text): ?><p class="our-story__description text-white"><?php echo $os_text; ?></p><?php endif; ?>
            <?php if($os_link): ?><a href="<?php echo $os_link; ?>"><button type="button" class="btn btn-white"><?php _e('Read more', 'base'); ?></button></a><?php endif; ?>
        </div>
    </section>
    <?php endif; ?>
<?php get_footer(); ?>