<?php
/*
Template Name: About
*/
get_header(); the_post();
$title = get_field('title');
$subtitle = get_field('subtitle');
$right_image = get_field('right_image');
?>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs hide-mobile">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>

    <!-- Content
    ================================================== -->
    <header class="about-header">
        <div class="about-header__background" style="background-image: url(<?php echo the_post_thumbnail_url('img_2000x1333') ?>); background-position: bottom center"></div>
        <div class="about-header__overlay"></div>
        <div class="about-header__content">
            <div class="container">
                <div class="row">

                    <!-- Text
                    ================================================== -->
                    <div class="col-sm-7 text-white">
                        <?php if($title): ?><h1><?php echo $title ?></h1><?php endif; ?>
                        <?php if($subtitle): ?><h3><?php echo $subtitle; ?></h3><?php endif; ?>
                        <div class="vert-spacer-10"></div>
                        <?php the_content() ?>
                    </div>

                    <div class="vert-spacer-20 visible-xs"></div>

                    <!-- Image
                    ================================================== -->
                    <?php if($right_image): ?>
                    <div class="col-sm-5 about-header__image">
                        <?php echo wp_get_attachment_image($right_image['id'], 'img_1000x664'); ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>

<?php get_footer(); ?>