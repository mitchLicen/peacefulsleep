<?php
/*
Template Name: Contacts
*/
get_header(); the_post();
$pin_image = get_field('pin_image');
$map_lat = get_field('map_lat');
$map_lng = get_field('map_lng');
$map_style = get_field('map_style');
$contacts_count = count(get_field('contacts'));
$form_title = get_field('form_title');
$form = get_field('form');
?>

    <!-- Map header
    ================================================== -->
    <header class="map">
        <div id="map"></div>
    </header>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>

    <!-- Content
    ================================================== -->
    <section>
        <div class="container">
            <div class="row">
                <!-- Contact details -->
                <div class="col-sm-5 col-md-4">
                    <?php $i=1; if( have_rows('contacts') ): ?>
                        <?php while ( have_rows('contacts') ) : the_row();
                            $title = get_sub_field('title'); ?>
                            <?php if($title): ?><h1 class="text-blue"><?php echo $title; ?></h1><?php endif; ?>
                            <?php if( have_rows('data') ): ?>
                                <?php while ( have_rows('data') ) : the_row();
                                    if( get_row_layout() == 'text' ):
                                        the_sub_field('text');
                                    elseif( get_row_layout() == 'phone_email' ):
                                        $phone = get_sub_field('phone');
                                        $email = get_sub_field('email'); ?>
                                        <p><?php if($phone): ?>T: <a href="tel:<?php echo clean_phone($phone); ?>"><?php echo $phone; ?></a><br><?php endif; ?>
                                            <?php if($email): ?>E: <a href="mailto:<?php echo antispambot($email); ?>"><?php echo antispambot($email); ?></a><?php endif; ?>
                                        </p>
                                    <?php endif;
                                endwhile; ?>
                            <?php endif; ?>
                            <?php if($i++ != $contacts_count): ?>
                            <div class="vert-spacer-20"></div>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>

                <div class="vert-spacer-20 visible-xs"></div>

                <!-- Form -->
                <div class="col-sm-7 com-md-8">
                    <?php if($form_title): ?><h1 class="text-blue"><?php echo $form_title; ?></h1><?php endif; ?>
                    <?php if($form) echo do_shortcode('[contact-form-7 id="'. $form .'"]'); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Google maps
    ================================================== -->
    <?php include(locate_template('blocks/google-maps.php')); ?>

<?php get_footer(); ?>