<?php
/*
Template Name: FAQ
*/
get_header(); the_post();
$faq_list_title = get_field('faq_list_title');
$faq_list = get_field('faq_list');
$title = get_field('title');
$text = get_field('text');
$button_title = get_field('button_title');
$button_link = get_field('button_link'); ?>

    <!-- Page header
    ================================================== -->
    <header class="header">
        <div class="header__background" style="background-image: url(<?php echo the_post_thumbnail_url('img_2000x1333') ?>); background-position: center center"></div>
        <div class="header__overlay"></div>
        <h1 class="header__title text-white"></h1>
    </header>


    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Content
    ================================================== -->
    <section>
        <div class="container">
            <div class="row">

                <!-- FAQ list -->
                <div class="col-sm-7 faq-list">
                    <?php if($faq_list_title): ?><h1 class="text-blue"><?php echo $faq_list_title; ?></h1><?php endif; ?>
                    <?php if($faq_list): ?>
                    <ul class="accordion">
                        <?php foreach ($faq_list as $faq_item): ?>
                        <li>
                            <h4 class="text-blue"><i class="icon ion-android-add-circle"></i> <?php echo $faq_item['question']; ?></h4>
                            <div class="content">
                                <?php echo $faq_item['answer']; ?>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>

                <!-- Title -->
                <div class="col-sm-5">
                    <?php if($title): ?><h1 class="text-blue"><?php echo $title; ?></h1><?php endif; ?>
                    <?php echo $text; ?>
                    <?php if($button_link): ?>
                    <a href="<?php echo $button_link; ?>"><button type="button" class="btn btn-blue"><?php echo $button_title; ?></button></a>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </section>
<?php get_footer(); ?>