<?php get_header();

global $paged, $wp_query;

$prev_posts_url = $next_posts_url = '';
if(is_category()) {
    $term = get_queried_object();
    $category_ids = new wpse_99513_adjacent_category($term->taxonomy, 'id');
    if ($next_category = $category_ids->next($term->term_id))
        $next_posts_url = get_term_link($next_category);

    if ($previous_category = $category_ids->previous($term->term_id))
        $prev_posts_url = get_term_link($previous_category);
}
?>

    <!-- Featured blog post
    ================================================== -->
    <header class="blog-header">
        <?php
        global $wp_query;
        $args = $wp_query->query_vars;
        $q = new WP_Query(array_merge($wp_query->query_vars, array('posts_per_page' => 1, 'paged' => 1, 'post__not_in' => '')));
        if ($q->have_posts()) : $q->the_post(); ?>
            <?php get_template_part( 'blocks/content-featured', get_post_type() ); ?>
        <?php wp_reset_query(); endif; ?>
        <?php if($prev_posts_url): ?><div class="nav-arrow nav-arrow__left" data-link="<?php echo $prev_posts_url; ?>"><i class="material-icons">chevron_left</i></div><?php endif; ?>
        <?php if($next_posts_url): ?><div class="nav-arrow nav-arrow__right" data-link="<?php echo $next_posts_url; ?>"><i class="material-icons">chevron_right</i></div><?php endif; ?>
    </header>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Post list
    ================================================== -->
    <section>
        
        <div class="container">
            <div class="row">

                <!-- Posts -->
                <div class="col-sm-9 col-sm-push-3">
                    <div class="row">
                        <?php if ( have_posts() ) : ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php get_template_part( 'blocks/content', get_post_type() ); ?>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <p><?php _e('No other posts exist.', 'base'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>

                <!-- Side widget -->
                <div class="col-sm-3 col-sm-pull-9 widget-sticky">
                    <?php dynamic_sidebar('left_posts') ?>
                </div>

            </div>

            <?php if($prev_posts_url || $next_posts_url): ?>
                <div class="product-list">
                    <div class="vert-spacer-50"></div>

                    <!-- Pager
                    ================================================== -->
                    <div class="col-sm-12 bottom-category-btns">
                        <div class="row">
                            <?php if($prev_posts_url): ?><a href="<?php echo $prev_posts_url; ?>" class="__left"><button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> <?php echo $previous_category->name; ?></button></a><?php endif; ?>
                            <?php if($next_posts_url): ?><a href="<?php echo $next_posts_url; ?>" class="__right"><button type="button" class="btn btn-ghost"><?php echo $next_category->name; ?> <i class="material-icons">chevron_right</i></button></a><?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>

<?php get_footer(); ?>