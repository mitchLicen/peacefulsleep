<?php
$prev_link = get_previous_posts_link( __('<button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> Previous Page</button>', 'base') );
$next_link = get_next_posts_link( __('<button type="button" class="btn btn-ghost">Next Page <i class="material-icons">chevron_right</i></button>', 'base') );
?>
<?php if($prev_link || $next_link): ?>
<div class="product-list">
	<div class="vert-spacer-50"></div>

	<!-- Pager
	================================================== -->
	<div class="col-sm-12 bottom-category-btns">
		<div class="row">
			<?php if($prev_link): ?><div class="__left"><?php echo $prev_link; ?></div><?php endif; ?>
			<?php if($next_link): ?><div class="__right"><?php echo $next_link; ?></div><?php endif; ?>
		</div>
	</div>
</div>
<?php endif; ?>