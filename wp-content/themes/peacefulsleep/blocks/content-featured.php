<?php
$categories = get_the_term_list(get_the_ID(), 'category', '', ', ');
$subtitle = get_field('subtitle');
$featured_product = get_field('featured_product');
$overlay_color = get_field('featured_overlay_color');
$overlay_opacity = get_field('featured_overlay_opacity');
$title_color = get_field('featured_title_color');
$subtitle_color = get_field('featured_subtitle_color');
$button_title = get_field('featured_button_title');
$button_color = get_field('featured_button_color');
$category_tag_theme = get_field('featured_category_tag_theme');
?>
<div class="featured-post__container">
    <div class="featured-post__background" style="background-image: url(<?php the_post_thumbnail_url('img_2000x1333'); ?>)"></div>
    <div class="featured-post__mossies"></div>
    <div class="featured-post__mask-image" style="background-image: url(<?php the_post_thumbnail_url('img_2000x1333'); ?>)"></div>
    <div class="featured-post__overlay"<?php if($overlay_color): ?> style="background:<?php echo hexToRgb($overlay_color, $overlay_opacity); ?>;"<?php endif; ?>></div>
    <div class="featured-post__frame"></div>
    <div class="featured-post__content">
        <div class="featured-post__category <?php echo $category_tag_theme; ?>"><?php echo $categories; ?></div>
        <h1 class="featured-post__title <?php echo $title_color; ?>"><?php the_title() ?></h1>
        <?php if($subtitle): ?><h4 class="featured-post__subtitle <?php echo $subtitle_color; ?>"><?php echo $subtitle; ?></h4><?php endif; ?>
        <a href="<?php echo the_permalink(); ?>"><button type="button" class="btn <?php echo $button_color; ?>"><?php _e($button_title, 'base'); ?></button></a>
    </div>
    <?php if($featured_product): ?>
				<div class="featured-post__feature-product">
						<?php echo get_the_post_thumbnail($featured_product[0], 'full'); ?>
				</div>
		<?php endif; ?>
</div>
<div class="featured-post__date">
    <span class="month"><?php echo the_time('M'); ?></span>
    <span class="date"><?php echo the_time('d'); ?></span>
    <span class="year"><?php echo the_time('Y'); ?></span>
</div>
