<!-- Where to buy
================================================== -->
<?php
$q = new WP_Query(array(
    'post_type' => 'where_to_buy',
    'posts_per_page' => -1,
));
if ($q->have_posts()) : ?>
    <section class="where-to-buy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="text-blue"><?php _e('Where to buy?', 'base'); ?></h1>
                </div>
                <div class="col-sm-12 no-hor-padding">
                    <div class="owl-carousel" id="retailers-owl">
                        <?php while ($q->have_posts()) : $q->the_post();
                        $external_link = get_field('external_link'); ?>
                            <?php if($external_link): ?><a href="<?php echo $external_link; ?>" target="_blank"><?php endif; ?>
                                <?php the_post_thumbnail('full') ?>
                            <?php if($external_link): ?></a><?php endif; ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php wp_reset_query(); endif; ?>