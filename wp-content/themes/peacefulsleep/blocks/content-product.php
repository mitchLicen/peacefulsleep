<div class="col-sm-12 col-md-6">
    <div class="card product">
        <div class="card__container">
            <?php if(has_post_thumbnail()): ?>
                <div class="card__image">
                    <?php the_post_thumbnail('full') ?>
                </div>
            <?php endif; ?>
            <div class="card__heading"><h4 class="text-blue"><?php the_title(); ?></h4></div>
            <div class="card__description"><?php echo get_the_excerpt(); ?></div>
            <div class="card__button">
                <a href="<?php the_permalink() ?>"><button type="button" class="btn btn-blue"><?php _e('More info', 'base'); ?></button></a>
            </div>
        </div>
    </div>
</div>