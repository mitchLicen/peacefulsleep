<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1_CE5017bKmCN_q4YpFSvn7dUMt2IMRA&callback=initMap" async defer></script>
<script type="text/javascript">
	var map;
	var lat = <?php echo $map_lat; ?>;
	var lng = <?php echo $map_lng; ?>;
	var myLatLng = {lat: lat, lng: lng};
	var mapStyle = <?php echo $map_style; ?>;


	function initMap() {
		map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			zoom: 15,
			scrollwheel: false,
			mapTypeControl: false,
			styles: mapStyle,
		});
		var image = {
			url: '<?php echo get_template_directory_uri(); ?>/images/map-marker.svg',
			size: new google.maps.Size(28, 28),
		};
		var image2 = '<?php echo $pin_image['url']; ?>';
        //var image = image2 ? image2 : image;
        var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Tiger Consumer Brands',
			icon: image2 ? image2 : image
		});
	}
</script>