<?php
$image = get_field('image', $product_term);
$list_title = get_field('list_title', $product_term); ?>
<div class="card">
    <div class="card__container">
        <?php if($image): ?>
            <div class="card__image">
                <?php echo wp_get_attachment_image($image['id'], 'full'); ?>
            </div>
        <?php endif; ?>
        <div class="card__heading"><h3 class="text-blue"><?php echo $list_title ? $list_title : $product_term->name; ?></h3></div>
        <div class="card__description"><?php echo $product_term->description; ?></div>
        <div class="card__button">
            <a href="<?php echo get_term_link($product_term); ?>"><button type="button" class="btn btn-blue"><?php _e('See products', 'base'); ?></button></a>
        </div>
    </div>
</div>