<?php
$categories = get_the_term_list(get_the_ID(), 'category', '', ', ');
$overlay_color = get_field('featured_overlay_color');
$overlay_opacity = get_field('featured_overlay_opacity');
$title_color = get_field('featured_title_color');
$subtitle_color = get_field('featured_subtitle_color');
$button_title = get_field('featured_button_title');
$button_color = get_field('featured_button_color');
$category_tag_theme = get_field('featured_category_tag_theme');
?>
<div class="col-sm-12 col-md-6">
    <div class="post-card">
        <div class="__background" style="background-image: url(<?php the_post_thumbnail_url('img_1500x'); ?>)"></div>
        <div class="__overlay"></div>
        <div class="__content">
            <div class="__category <?php echo $category_tag_theme; ?>"><?php echo $categories; ?></div>
            <div class="__title <?php echo $title_color; ?>"><h1><?php the_title() ?></h1></div>
            <a href="<?php the_permalink() ?>"><button type="button" class="btn <?php echo $button_color; ?>"><?php _e($button_title, 'base'); ?></button></a>
        </div>
        <div class="__date">
            <span class="month"><?php echo the_time('M'); ?></span>
            <span class="date"><?php echo the_time('d'); ?></span>
            <span class="year"><?php echo the_time('Y'); ?></span>
        </div>
    </div>
</div>