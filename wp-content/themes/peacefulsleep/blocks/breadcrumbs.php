<?php if(function_exists('bcn_display')) : ?>
    <div class="row">
        <div class="container">
            <ol class="breadcrumb">
                <?php bcn_display_list(); ?>
            </ol>
        </div>
    </div>
<?php endif; ?>