<!-- Which product is right for me
================================================== -->
<?php
$hid = get_option('page_on_front');
$q_start_image = get_field('q_start_image', $hid);
$q_start_text = get_field('q_start_text', $hid);
$q_start_button_title = get_field('q_start_button_title', $hid);
$q_end_button_title = get_field('q_end_button_title', $hid);
$q_product_block_title = get_field('q_product_block_title', $hid);
?>
<section class="which-product no-vert-padding">
	<div class="container">

		<?php if($q_start_text): ?>
		<!-- Intro
		================================================== -->
		<div class="col-sm-12" id="wps-intro" data-show="1" data-index="1">
			<div class="col-sm-4 text-left __image">
                <?php echo wp_get_attachment_image($q_start_image['id'], 'full'); ?>
			</div>

			<div class="col-sm-8 __rightbox">
				<h1 class="text-blue"><?php echo $q_start_text; ?></h1>
				<div class="vert-spacer-10"></div>
				<button type="button" class="btn btn-blue" data-goto="next"><?php echo $q_start_button_title ? $q_start_button_title : 'Start'; ?></button>
			</div>
		</div>
        <?php endif; ?>

        <?php if( have_rows('questions', $hid) ): ?>
            <?php while ( have_rows('questions', $hid) ) : the_row();
                $question_id = get_sub_field('question_id');
                $question = get_sub_field('question'); ?>
                <div class="col-sm-12" data-show="0" data-index="<?php echo $question_id; ?>">
                    <div class="text-center text-blue">
                        <h2><?php echo $question; ?></h2>
                        <?php if( have_rows('answers') ): ?>
                            <?php while ( have_rows('answers') ) : the_row();
                                $text = get_sub_field('text');
                                $recommend_result = get_sub_field('recommend_result');
                                $go_to_question_id = get_sub_field('go_to_question_id'); ?>
                                <button type="button" class="btn btn-blue" data-goto="<?php echo $go_to_question_id ? $go_to_question_id : 'results'; ?>" data-value="<?php echo $recommend_result->slug; ?>"><?php echo $text; ?></button>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>

		<!-- Results
		================================================== -->
		<div class="col-sm-12" id="wps-results" data-show="0" data-index="results">

            <?php if($q_product_block_title): ?>
			<div class="col-sm-12">
				<h4 class="text-blue text-center"><?php echo $q_product_block_title; ?></h4>
			</div>
            <?php endif; ?>
			<div class="col-sm-12 results ajax-product-results-holder">
                <div class="ajax-product-results"></div>
				<div class="pagination-dots" id="which-product-dots"></div>
				<div class="row text-center btm-buttons">
					<button type="button" class="btn btn-blue restart" data-goto="1"><?php echo $q_end_button_title ? $q_end_button_title : 'Start over'; ?></button>
				</div>
			</div>
			</div>
		
		</div>
</section>