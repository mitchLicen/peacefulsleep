<?php
    $prev = get_previous_post_link( __( '%link', 'base' ), __('<button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> %title</button>', 'base'), true );
    if(!$prev){
        $prev = get_adjacent_post_link_custom( __( '%link', 'base' ), __('<button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> %title</button>', 'base'), true );
    }
    $next = get_next_post_link( __( '%link', 'base' ), __('<button type="button" class="btn btn-ghost">%title <i class="material-icons">chevron_right</i></button>', 'base'), true );
    if(!$next){
        $next = get_adjacent_post_link_custom( __( '%link', 'base' ), __('<button type="button" class="btn btn-ghost">%title <i class="material-icons">chevron_right</i></button>', 'base'), true, '', false );
    }
?>
<?php if( $prev || $next ) : ?>
    <div class="product-list">
        <div class="vert-spacer-50"></div>

        <!-- Pager
        ================================================== -->
        <div class="col-sm-12 bottom-category-btns">
            <div class="row">
                <?php if( $prev ) : ?><div class="__left"><?php echo $prev ?></div><?php endif ?>
                <?php if( $next ) : ?><div class="__right"><?php echo $next ?></div><?php endif ?>
            </div>
        </div>
    </div>
<?php endif ?>