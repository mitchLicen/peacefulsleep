<?php
//error_reporting(E_ALL);

//Additional classes
include( get_template_directory() . '/inc/classes.php' );

// Custom Widgets
include( get_template_directory() . '/inc/widgets.php' );

// Theme sidebars

function theme_widget_init() {
    register_sidebar( array(
        'id'            => 'left_products',
        'name'          => __( 'Left Products', 'base' ),
        'before_widget' => '<div class="side-widget left %2$s" id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="title">',
        'after_title'   => '</div>'
    ) );
    register_sidebar( array(
        'id'            => 'left_products_tag',
        'name'          => __( 'Left Products Tag', 'base' ),
        'before_widget' => '<div class="side-widget left %2$s" id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="title">',
        'after_title'   => '</div>'
    ) );
    register_sidebar( array(
        'id'            => 'left_posts',
        'name'          => __( 'Left Posts', 'base' ),
        'before_widget' => '<div class="side-widget-blog left %2$s" id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="title">',
        'after_title'   => '</div>'
    ) );
    register_sidebar( array(
        'id'            => 'single_post',
        'name'          => __( 'Single Post', 'base' ),
        'before_widget' => '<div class="side-widget-blog right %2$s" id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="title">',
        'after_title'   => '</div>'
    ) );
    register_sidebar( array(
        'id'            => 'search',
        'name'          => __( 'Search', 'base' ),
        'before_widget' => '<div class="side-widget-blog right %2$s" id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="title">',
        'after_title'   => '</div>'
    ) );
}
add_action( 'widgets_init', 'theme_widget_init' );

// Theme thumbnails

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 150, 150, true ); // Normal post thumbnails
add_image_size( 'img_2000x1333', 2000, 1333, true );
add_image_size( 'img_1500x', 1500, 9999, true );
add_image_size( 'img_1000x664', 1000, 664, true );

// Theme menus

register_nav_menus( array(
    'nav' => __( 'Navigation', 'base' ),
) );

// Theme css & js

function base_scripts_styles() {
    $in_footer = true;
    /*
     * Adds JavaScript to pages with the comment form to support
     * sites with threaded comments (when in use).
     */
    wp_deregister_script( 'comment-reply' );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply', '/wp-includes/js/comment-reply.js', '', '', $in_footer );
        //wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js', '', '', $in_footer );
    }

    // Loads JavaScript file with functionality specific.
    wp_enqueue_script( 'ps-js-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-TweenMax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-stickyfill', 'https://cdnjs.cloudflare.com/ajax/libs/stickyfill/1.1.4/stickyfill.min.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-ScrollMagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-ScrollMagic-debug', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-OwlCarousel2', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-ScrollMagic-animation', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js', array( 'jquery' ), '', $in_footer );
    wp_enqueue_script( 'ps-js-skrollr', get_template_directory_uri() . '/js/peaceful-sleep.js', array( 'jquery' ), '', $in_footer );

    wp_enqueue_script( 'ps-js-html5shiv', 'https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.js', array(), '3.7.2', false );
    wp_script_add_data( 'ps-js-html5shiv', 'conditional', 'lt IE 9' );

    wp_enqueue_script( 'ps-js-respond', 'https://oss.maxcdn.com/respond/1.4.2/respond.min.js', array(), '1.4.2', false );
    wp_script_add_data( 'ps-js-respond', 'conditional', 'lt IE 9' );

    // Implementation stylesheet.
    wp_enqueue_style( 'ps-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array() );
    wp_enqueue_style( 'ps-googleapis-1', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700', array() );
    wp_enqueue_style( 'ps-googleapis-2', 'https://fonts.googleapis.com/css?family=Dosis', array() );
    wp_enqueue_style( 'ps-googleapis-3', 'https://fonts.googleapis.com/icon?family=Material+Icons', array() );
    wp_enqueue_style( 'ps-ionicons', 'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', array() );
    wp_enqueue_style( 'ps-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array() );
    wp_enqueue_style( 'ps-animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css', array() );
    wp_enqueue_style( 'ps-owl', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css', array() );
    wp_enqueue_style( 'ps-peaceful-sleep', get_template_directory_uri() . '/css/peaceful-sleep.min.css', array() );

    wp_enqueue_style( 'my-theme-old-ie', get_stylesheet_directory_uri() . "/css/old-ie.css", array( 'my-theme' ) );
    wp_style_add_data( 'my-theme-old-ie', 'conditional', 'lt IE 9' );

    // Loads our main stylesheet.
    wp_enqueue_style( 'base-style', get_stylesheet_uri(), array() );
}
add_action( 'wp_enqueue_scripts', 'base_scripts_styles' );

// Default theme settings

// Custom logo support
function theme_add_logo_support() {
	add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'theme_add_logo_support' );

/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

//Add [email]...[/email] shortcode
function shortcode_email( $atts, $content ) {
    return antispambot( $content );
}
add_shortcode( 'email', 'shortcode_email' );

//Register tag [template-url]
function filter_template_url( $text ) {
    return str_replace( '[template-url]', get_template_directory_uri(), $text );
}
add_filter( 'the_content', 'filter_template_url' );
add_filter( 'widget_text', 'filter_template_url' );

//Register tag [site-url]
function filter_site_url( $text ) {
    return str_replace( '[site-url]', home_url(), $text );
}
add_filter( 'the_content', 'filter_site_url' );
add_filter( 'widget_text', 'filter_site_url' );

//Replace standard wp menu classes
function change_menu_classes( $css_classes ) {
    return str_replace( array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' ), 'active', $css_classes );
}
add_filter( 'nav_menu_css_class', 'change_menu_classes' );

//Allow tags in category description
$filters = array( 'pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description' );
foreach ( $filters as $filter ) {
    remove_filter( $filter, 'wp_filter_kses' );
}

//Disable comments on pages by default
function theme_page_comment_status( $post_ID, $post, $update ) {
    if ( !$update ) {
        remove_action( 'save_post_page', 'theme_page_comment_status', 10 );
        wp_update_post( array(
            'ID' => $post->ID,
            'comment_status' => 'closed',
        ) );
        add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );
    }
}
add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );

//theme options tab in appearance
if( function_exists( 'acf_add_options_sub_page' ) && current_user_can( 'manage_options' ) ) {
    acf_add_options_sub_page( array(
        'title'  => 'Theme Options',
        'parent' => 'themes.php',
    ) );
}

//acf theme functions placeholders
if( !class_exists( 'acf' ) && !is_admin() ) {
    function get_field_reference( $field_name, $post_id ) { return ''; }
    function get_field_objects( $post_id = false, $options = array() ) { return false; }
    function get_fields( $post_id = false ) { return false; }
    function get_field( $field_key, $post_id = false, $format_value = true )  { return false; }
    function get_field_object( $field_key, $post_id = false, $options = array() ) { return false; }
    function the_field( $field_name, $post_id = false ) {}
    function have_rows( $field_name, $post_id = false ) { return false; }
    function the_row() {}
    function reset_rows( $hard_reset = false ) {}
    function has_sub_field( $field_name, $post_id = false ) { return false; }
    function get_sub_field( $field_name ) { return false; }
    function the_sub_field( $field_name ) {}
    function get_sub_field_object( $child_name ) { return false;}
    function acf_get_child_field_from_parent_field( $child_name, $parent ) { return false; }
    function register_field_group( $array ) {}
    function get_row_layout() { return false; }
    function acf_form_head() {}
    function acf_form( $options = array() ) {}
    function update_field( $field_key, $value, $post_id = false ) { return false; }
    function delete_field( $field_name, $post_id ) {}
    function create_field( $field ) {}
    function reset_the_repeater_field() {}
    function the_repeater_field( $field_name, $post_id = false ) { return false; }
    function the_flexible_field( $field_name, $post_id = false ) { return false; }
    function acf_filter_post_id( $post_id ) { return $post_id; }
}

function clean_phone( $phone ){
    return preg_replace( '/[^0-9]/', '', $phone );
}

function get_custom_logo_url(){
    $html = '';
    $custom_logo_id = get_theme_mod( 'custom_logo' );

    // We have a logo. Logo is go.
    if ( $custom_logo_id ) {
        $html = wp_get_attachment_image_url( $custom_logo_id, 'full' );
   }
   return $html;
}

function hexToRgb($hex, $alpha = false) {
   $hex      = str_replace('#', '', $hex);
   $length   = strlen($hex);
   $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
   $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
   $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
   if ( $alpha ) {
      $rgb['a'] = $alpha;
   }
   return sprintf('rgba(%s, %s, %s%s)', $rgb['r'], $rgb['g'], $rgb['b'], ($rgb['a'] ? ', '.$rgb['a'] : ''));
}

//exclude featureed post from main posts query
function posts_query_hook($query) {
    if (($query->is_home() || $query->is_category() || $query->is_tag() || $query->is_author() || $query->is_date()) &&
        $query->is_main_query() && !is_admin()) {
        $q = get_posts(array_merge($query->query_vars, array('posts_per_page' => 1, 'paged' => 1)));
        if(isset($q[0]->ID)){
            $query->set('post__not_in', array($q[0]->ID));
        }
    }
}
add_action('pre_get_posts', 'posts_query_hook');

//search exclude pages
add_filter('register_post_type_args', function($args, $post_type) {
    if (!is_admin() && $post_type == 'page') {
        $args['exclude_from_search'] = true;
    }
    return $args;
}, 10, 2);

//remove limit from product list
function products_query_hook($query) {
    if ($query->is_tax('product_category') && $query->is_main_query() && !is_admin()) {
        $query->set('posts_per_page', -1);
    }
}
//add_action('pre_get_posts', 'products_query_hook');

//Plugin Name: Image P tag remover
function filter_ptags_on_images($content)
{
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .*>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

function get_first_last_post( $in_same_term = false, $excluded_terms = '', $previous = true, $taxonomy = 'category' ) {
    global $wpdb;

    if ( ( ! $post = get_post() ) || ! taxonomy_exists( $taxonomy ) )
        return null;

    $current_post_date = $post->post_date;

    $join = '';
    $where = '';
    $adjacent = $previous ? 'previous' : 'next';

    if ( $in_same_term || ! empty( $excluded_terms ) ) {
        if ( ! empty( $excluded_terms ) && ! is_array( $excluded_terms ) ) {
            // back-compat, $excluded_terms used to be $excluded_terms with IDs separated by " and "
            if ( false !== strpos( $excluded_terms, ' and ' ) ) {
                _deprecated_argument( __FUNCTION__, '3.3.0', sprintf( __( 'Use commas instead of %s to separate excluded terms.' ), "'and'" ) );
                $excluded_terms = explode( ' and ', $excluded_terms );
            } else {
                $excluded_terms = explode( ',', $excluded_terms );
            }

            $excluded_terms = array_map( 'intval', $excluded_terms );
        }

        if ( $in_same_term ) {
            $join .= " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";
            $where .= $wpdb->prepare( "AND tt.taxonomy = %s", $taxonomy );

            if ( ! is_object_in_taxonomy( $post->post_type, $taxonomy ) )
                return '';
            $term_array = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );

            // Remove any exclusions from the term array to include.
            $term_array = array_diff( $term_array, (array) $excluded_terms );
            $term_array = array_map( 'intval', $term_array );

            if ( ! $term_array || is_wp_error( $term_array ) )
                return '';

            $where .= " AND tt.term_id IN (" . implode( ',', $term_array ) . ")";
        }

        /**
         * Filters the IDs of terms excluded from adjacent post queries.
         *
         * The dynamic portion of the hook name, `$adjacent`, refers to the type
         * of adjacency, 'next' or 'previous'.
         *
         * @since 4.4.0
         *
         * @param string $excluded_terms Array of excluded term IDs.
         */
        $excluded_terms = apply_filters( "get_{$adjacent}_post_excluded_terms", $excluded_terms );

        if ( ! empty( $excluded_terms ) ) {
            $where .= " AND p.ID NOT IN ( SELECT tr.object_id FROM $wpdb->term_relationships tr LEFT JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id) WHERE tt.term_id IN (" . implode( ',', array_map( 'intval', $excluded_terms ) ) . ') )';
        }
    }

    // 'post_status' clause depends on the current user.
    if ( is_user_logged_in() ) {
        $user_id = get_current_user_id();

        $post_type_object = get_post_type_object( $post->post_type );
        if ( empty( $post_type_object ) ) {
            $post_type_cap    = $post->post_type;
            $read_private_cap = 'read_private_' . $post_type_cap . 's';
        } else {
            $read_private_cap = $post_type_object->cap->read_private_posts;
        }

        /*
         * Results should include private posts belonging to the current user, or private posts where the
         * current user has the 'read_private_posts' cap.
         */
        $private_states = get_post_stati( array( 'private' => true ) );
        $where .= " AND ( p.post_status = 'publish'";
        foreach ( (array) $private_states as $state ) {
            if ( current_user_can( $read_private_cap ) ) {
                $where .= $wpdb->prepare( " OR p.post_status = %s", $state );
            } else {
                $where .= $wpdb->prepare( " OR (p.post_author = %d AND p.post_status = %s)", $user_id, $state );
            }
        }
        $where .= " )";
    } else {
        $where .= " AND p.post_status = 'publish'";
    }

    $op = $previous ? '<' : '>';
    $order = $previous ? 'DESC' : 'ASC';

    /**
     * Filters the JOIN clause in the SQL for an adjacent post query.
     *
     * The dynamic portion of the hook name, `$adjacent`, refers to the type
     * of adjacency, 'next' or 'previous'.
     *
     * @since 2.5.0
     * @since 4.4.0 Added the `$taxonomy` and `$post` parameters.
     *
     * @param string  $join           The JOIN clause in the SQL.
     * @param bool    $in_same_term   Whether post should be in a same taxonomy term.
     * @param array   $excluded_terms Array of excluded term IDs.
     * @param string  $taxonomy       Taxonomy. Used to identify the term used when `$in_same_term` is true.
     * @param WP_Post $post           WP_Post object.
     */
    $join = apply_filters( "get_{$adjacent}_post_join", $join, $in_same_term, $excluded_terms, $taxonomy, $post );

    /**
     * Filters the WHERE clause in the SQL for an adjacent post query.
     *
     * The dynamic portion of the hook name, `$adjacent`, refers to the type
     * of adjacency, 'next' or 'previous'.
     *
     * @since 2.5.0
     * @since 4.4.0 Added the `$taxonomy` and `$post` parameters.
     *
     * @param string $where          The `WHERE` clause in the SQL.
     * @param bool   $in_same_term   Whether post should be in a same taxonomy term.
     * @param array  $excluded_terms Array of excluded term IDs.
     * @param string $taxonomy       Taxonomy. Used to identify the term used when `$in_same_term` is true.
     * @param WP_Post $post           WP_Post object.
     */
    $where = apply_filters( "get_{$adjacent}_post_where", $wpdb->prepare( "WHERE p.id NOT IN (%d) AND p.post_type = %s $where", $post->ID, $post->post_type ), $in_same_term, $excluded_terms, $taxonomy, $post );

    /**
     * Filters the ORDER BY clause in the SQL for an adjacent post query.
     *
     * The dynamic portion of the hook name, `$adjacent`, refers to the type
     * of adjacency, 'next' or 'previous'.
     *
     * @since 2.5.0
     * @since 4.4.0 Added the `$post` parameter.
     *
     * @param string $order_by The `ORDER BY` clause in the SQL.
     * @param WP_Post $post    WP_Post object.
     */
    $sort  = apply_filters( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1", $post );

    $query = "SELECT p.ID FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5( $query );
    $result = wp_cache_get( $query_key, 'counts' );
    if ( false !== $result ) {
        if ( $result )
            $result = get_post( $result );
        return $result;
    }

    $result = $wpdb->get_var( $query );
    if ( null === $result )
        $result = '';

    wp_cache_set( $query_key, $result, 'counts' );

    if ( $result )
        $result = get_post( $result );

    return $result;
}

function get_adjacent_post_link_custom( $format, $link, $in_same_term = false, $excluded_terms = '', $previous = true, $taxonomy = 'category' ) {
    if ( $previous && is_attachment() )
        $post = get_post( get_post()->post_parent );
    else
        $post = get_first_last_post( $in_same_term, $excluded_terms, $previous, $taxonomy );

    if ( ! $post ) {
        $output = '';
    } else {
        $title = $post->post_title;

        if ( empty( $post->post_title ) )
            $title = $previous ? __( 'Previous Post' ) : __( 'Next Post' );

        /** This filter is documented in wp-includes/post-template.php */
        $title = apply_filters( 'the_title', $title, $post->ID );

        $date = mysql2date( get_option( 'date_format' ), $post->post_date );
        $rel = $previous ? 'prev' : 'next';

        $string = '<a href="' . get_permalink( $post ) . '" rel="'.$rel.'">';
        $inlink = str_replace( '%title', $title, $link );
        $inlink = str_replace( '%date', $date, $inlink );
        $inlink = $string . $inlink . '</a>';

        $output = str_replace( '%link', $inlink, $format );
    }

    $adjacent = $previous ? 'previous' : 'next';

    /**
     * Filters the adjacent post link.
     *
     * The dynamic portion of the hook name, `$adjacent`, refers to the type
     * of adjacency, 'next' or 'previous'.
     *
     * @since 2.6.0
     * @since 4.2.0 Added the `$adjacent` parameter.
     *
     * @param string  $output   The adjacent post link.
     * @param string  $format   Link anchor format.
     * @param string  $link     Link permalink format.
     * @param WP_Post $post     The adjacent post.
     * @param string  $adjacent Whether the post is previous or next.
     */
    return apply_filters( "{$adjacent}_post_link", $output, $format, $link, $post, $adjacent );
}

function add_tax_to_permalinks( $post_link, $post ){
    if ( is_object( $post ) && $post->post_type == 'product' ){
        $terms = wp_get_object_terms( $post->ID, 'product_category' );
        if( $terms ){
            return str_replace( '%product_category%' , $terms[0]->slug , $post_link );
        }
    }
    return $post_link;
}
add_filter( 'post_type_link', 'add_tax_to_permalinks', 1, 2 );

//AJAX RESPONSES
add_action('wp','ajax_actions');
function ajax_actions(){
    if(isset($_GET['ajax'])){
        if(isset($_GET['results'])){
            $q = new WP_Query(array(
                'post_type' => 'product',
                //'posts_per_page' => 3,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'products_recommend_results',
                        'field'    => 'slug',
                        'terms'    => array_filter(explode(',', $_GET['results'])),
                        'operator' => 'AND'
                    ),
                ),
            ));
            ?>
            <?php if ($q->have_posts()) : ?>
                <div class="row" data-show="1">
                    <div class="owl-carousel">
                        <?php while ($q->have_posts()) : $q->the_post();
                            $product_category = get_the_term_list(get_the_ID(), 'product_category', '', ', '); ?>
                            <div class="product">
                                <?php the_post_thumbnail('full'); ?>
                                <div class="__heading"><a href="<?php the_permalink() ?>"><h4 class="text-blue"><?php the_title(); ?></h4></a></div>
                                <?php if($product_category): ?><div class="__tag"><?php echo $product_category; ?></div><?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            <?php endif;
        }
        die;
    }
}