<?php get_header(); the_post();

//Category data
$main_product_category = '';
$product_categories = get_the_terms(get_the_ID(), 'product_category');
$product_tags = get_the_terms(get_the_ID(), 'product_tag');
if($product_categories[0]){
    $main_product_category = $product_categories[0];
}
$page_image = get_field('page_image', $main_product_category);

//Post data
$main_post_id = get_the_ID();
$prev = get_previous_post_link( __( '%link', 'base' ), '<i class="icon ion-chevron-left"></i>', true, '', 'product_category' );
if(!$prev){
    $prev = get_adjacent_post_link_custom( __( '%link', 'base' ), '<i class="icon ion-chevron-left"></i>', true, '', true,'product_category' );
}
$next = get_next_post_link( __( '%link', 'base' ), '<i class="icon ion-chevron-right"></i>', true, '', 'product_category' );
if(!$next){
    $next = get_adjacent_post_link_custom( __( '%link', 'base' ), '<i class="icon ion-chevron-right"></i>', true, '', false,'product_category' );
}

$prev2 = get_previous_post_link( __( '%link', 'base' ), '<button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> prev product</button>', true, '', 'product_category' );
if(!$prev2){
    $prev2 = get_adjacent_post_link_custom( __( '%link', 'base' ), '<button type="button" class="btn btn-ghost"><i class="material-icons">chevron_left</i> prev product</button>', true, '', true,'product_category' );
}
$next2 = get_next_post_link( __( '%link', 'base' ), '<button type="button" class="btn btn-ghost">next product <i class="material-icons">chevron_right</i></button>', true, '', 'product_category' );
if(!$next2){
    $next2 = get_adjacent_post_link_custom( __( '%link', 'base' ), '<button type="button" class="btn btn-ghost">next product <i class="material-icons">chevron_right</i></button>', true, '', false,'product_category' );
}

$registration = get_field('registration');
$benefits = get_field('benefits');
$directions_for_use = get_field('directions_for_use');
?>
    <!-- Header
    ================================================== -->
    <?php if($main_product_category): ?>
    <header class="header">
        <?php if($page_image): ?><div class="header__background" style="background-image: url(<?php echo wp_get_attachment_image_url($page_image['id'], 'img_2000x1333'); ?>); background-position: top center;"></div><?php endif; ?>
        <div class="header__overlay"></div>
        <div class="header__category dark"><?php echo $main_product_category->name; ?> range</div>
    </header>
    <?php endif; ?>


    <!-- Breadcrumbs: Tablets/Desktops
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Content
    ================================================== -->
    <section class="product-detail">
        <div class="container">
            <div class="row">

                <!-- Product details
                ================================================== -->
                <div class="col-sm-9 col-sm-push-3">
                    <div class="row">

                        <!-- Image
                        ================================================== -->
                        <?php if(has_post_thumbnail()): ?>
                        <div class="col-sm-12 col-md-6">
                            <div class="product-image">
                                <?php the_post_thumbnail('full') ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <!-- Description
                        ================================================== -->
                        <div class="col-sm-12  col-md-6">

                            <!-- Product title -->
                            <h3 class="text-blue product-title">
                                <?php the_title(); ?>
                                <?php if( $prev || $next ) : ?>
                                <span style="float: right">
									<?php echo $prev; ?>
									<?php echo $next; ?>
								</span>
                                <?php endif; ?>
                            </h3>
                            <?php the_content() ?>

                            <?php if($product_tags): ?>
                            <!-- Ideal for -->
                            <p class="product-ideal-for">
                                <strong><?php _e('Ideal for', 'base'); ?></strong>
                                <?php foreach($product_tags as $tag) :
                                    $icon = get_field('icon', $tag);
																		$iconColor = get_field('icon_color', $tag);?>
                                    <i class="icon <?php echo $icon ?>" style="color:<?php echo $iconColor ?>" title="<?php echo $tag->name ?>" data-toggle="tooltip" data-placement="bottom"></i>
                                <?php endforeach; ?>
                            </p>
                            <?php endif; ?>

                            <!-- Small detail -->
                            <?php if($product_categories): ?><p><strong><?php _e('Category', 'base'); ?>: </strong><?php echo get_the_term_list(get_the_ID(), 'product_category', '', ', '); ?></p><?php endif; ?>
                            <?php if($product_tags): ?><p><strong><?php _e('Tags', 'base'); ?>: </strong><?php echo get_the_term_list(get_the_ID(), 'product_tag', '', ', '); ?></p><?php endif; ?>

                            <?php if($registration): ?><p class="small"><?php echo $registration; ?></p><?php endif; ?>
                        </div>

                        <!-- Additional info
                        ================================================== -->
                        <?php if($benefits || $benefits): ?>
                        <div class="col-sm-12 product-additional-info product-extra-section">
                            <h3><?php _e('Additional information', 'base'); ?></h3>
                            <ul class="accordion">
                                <?php if($benefits): ?>
                                <li>
                                    <h4><i class="icon ion-android-add-circle"></i> <?php _e('Benefits', 'base'); ?></h4>
                                    <div class="content">
                                        <ol>
                                            <?php foreach($benefits as $benefit) : ?>
                                                <li><?php echo $benefit['text'];?></li>
                                            <?php endforeach; ?>
                                        </ol>
                                    </div>
                                </li>
                                <?php endif; ?>
                                <?php if($directions_for_use): ?>
                                <li><h4><i class="icon ion-android-add-circle"></i> <?php _e('Directions for use', 'base'); ?></h4>
                                    <div class="content">
                                        <p><?php echo $directions_for_use; ?></p>
                                    </div>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <?php endif; ?>


                        <!-- Related products
                        ================================================== -->
                        <?php if($main_product_category): ?>
                            <?php
                            $q = new WP_Query(array(
                                'post_type' => 'product',
                                'posts_per_page' => 6,
                                'product_category' => $main_product_category->slug,
                                'orderby' => 'rand',
                                'post__not_in' => array($main_post_id)
                            ));
                            if ($q->have_posts()) : ?>
                            <div class="col-sm-12 product-related product-extra-section no-hor-padding">
                                <div class="col-sm-12">
                                    <h3><?php _e('Related products', 'base'); ?></h3>
                                </div>
                                <div class="col-sm-12">
                                    <div class="vert-spacer-20"></div>
                                    <div class="owl-carousel" id="related-products-slider">
                                        <?php while ($q->have_posts()) : $q->the_post();
                                            $product_category = get_the_term_list(get_the_ID(), 'product_category', '', ', '); ?>
                                            <div class="card product-related">
                                                <div class="card__container">
                                                    <?php if(has_post_thumbnail()): ?>
                                                    <div class="card__image">
                                                        <?php the_post_thumbnail('full') ?>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="card__heading"><a href="<?php the_permalink() ?>"><h4 class="text-blue"><?php the_title(); ?></h4></a></div>
                                                    <?php if($product_category): ?><div class="card__tag"><?php echo $product_category; ?></div><?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <div class="pagination-dots" id="related-products-dots"></div>
                                </div>
                            </div>
                            <?php endif; ?>
                        <?php wp_reset_query(); endif; ?>

                    </div>
                </div>

                <!-- Side widget
                ================================================== -->
                <div class="col-sm-3 col-sm-pull-9 widget-sticky">
                    <?php dynamic_sidebar('left_products') ?>
                </div>

            </div>

            <div class="vert-spacer-50"></div>

            <?php if( $prev2 || $next2 ) : ?>
            <!-- Next/Prev links
            ================================================== -->
            <div class="col-sm-12 bottom-category-btns">
                <div class="row">
                    <?php if( $prev2 ) : ?><div class="__left"><?php echo $prev2; ?></div><?php endif; ?>
                    <?php if( $next2 ) : ?><div class="__right"><?php echo $next2; ?></div><?php endif; ?>
                </div>
            </div>
            <?php endif; ?>

        </div>
    </section>

    <?php get_template_part( 'blocks/which-product' ); ?>

    <?php get_template_part( 'blocks/where-to-buy'); ?>
<?php get_footer(); ?>