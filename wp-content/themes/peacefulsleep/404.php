<?php get_header(); the_post(); ?>
    <!-- Page header
    ================================================== -->
    <header class="header">
        <div class="header__background" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/backgrounds/products.jpg'); background-position: bottom center"></div>
        <div class="header__overlay"></div>
        <h1 class="header__title text-white"><span class="label label-default" style="font-size: 12px">error 404</span><br><?php the_field('404_title', 'option') ?></h1>
    </header>


    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
									<h1><?php the_field('404_content', 'option') ?></h1>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>