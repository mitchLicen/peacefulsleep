<?php get_header(); ?>

    <!-- Page header
    ================================================== -->
    <header class="header">
        <div class="header__background" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/backgrounds/products.jpg'); background-position: bottom center"></div>
        <div class="header__overlay"></div>
        <h1 class="header__title text-white"><?php printf( __( 'Search Results for: %s', 'base' ), '<span>' . get_search_query() . '</span>'); ?></h1>
        <?php if($prev_posts_url): ?><div class="nav-arrow nav-arrow__left" data-link="<?php echo $prev_posts_url; ?>"><i class="material-icons">chevron_left</i></div><?php endif; ?>
        <?php if($next_posts_url): ?><div class="nav-arrow nav-arrow__right" data-link="<?php echo $next_posts_url; ?>"><i class="material-icons">chevron_right</i></div><?php endif; ?>
    </header>

    <!-- Breadcrumbs
    ================================================== -->
    <div class="col-sm-12 breadcrumbs">
        <?php get_template_part( 'blocks/breadcrumbs' ); ?>
    </div>


    <!-- Post list
    ================================================== -->
    <section class="section-results">
        <div class="container">
            <div class="row">

                <!-- Posts -->
                <div class="col-sm-9 col-sm-push-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="text-blue">Related products</h2>
                            <div class="vert-spacer-20"></div>
                        </div>
                        <?php
                        $qp = new WP_Query(array(
                            'post_type' => 'product',
                            'posts_per_page' => -1,
                            's' => get_search_query(),
                        ));
                        ?>
                        <div class="col-sm-12">
                            <div class="owl-carousel slick slick-slider">
                                <?php if ( $qp->have_posts() ) : ?>
                                    <?php while ( $qp->have_posts() ) : $qp->the_post(); ?>
                                        <div class="card product">
                                            <div class="card__container">
                                                <?php if(has_post_thumbnail()): ?>
                                                    <div class="card__image">
                                                        <?php the_post_thumbnail('full') ?>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="card__heading"><h4 class="text-blue"><?php the_title(); ?></h4></div>
                                                <div class="card__description"><?php echo get_the_excerpt(); ?></div>
                                                <div class="card__button">
                                                    <a href="<?php the_permalink() ?>"><button type="button" class="btn btn-blue"><?php _e('More info', 'base'); ?></button></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <p>Search has no results.</p>
                                <?php endif; ?>
                            </div>
                            <div class="slick-dots">
                                <div class="slick-prev"><i class="material-icons">chevron_left</i></div>
                                <div class="dots-holder"></div>
                                <div class="slick-next"><i class="material-icons">chevron_right</i></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <h2 class="text-blue">Related blog posts</h2>
                            <div class="vert-spacer-20"></div>
                        </div>
                        <?php
                        $qp2 = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page' => -1,
                            's' => get_search_query(),
                        )); ?>
                        <div class="col-sm-12">
                            <div class="owl-carousel slick1 slick-slider">
                                <?php if ( $qp2->have_posts() ) : ?>
                                    <?php while ( $qp2->have_posts() ) : $qp2->the_post();
                                        $categories = get_the_term_list(get_the_ID(), 'category', '', ', '); ?>
                                        <div class="post-card">
                                            <div class="__background" style="background-image: url(<?php the_post_thumbnail_url('img_1500x'); ?>)"></div>
                                            <div class="__overlay"></div>
                                            <div class="__content">
                                                <div class="__category light"><?php echo $categories; ?></div>
                                                <div class="__title"><h1 class="text-white"><?php the_title() ?></h1></div>
                                                <a href="<?php the_permalink() ?>"><button type="button" class="btn btn-white"><?php _e('Read more', 'base'); ?></button></a>
                                            </div>
                                            <div class="__date">
                                                <span class="month"><?php echo the_time('M'); ?></span>
                                                <span class="date"><?php echo the_time('d'); ?></span>
                                                <span class="year"><?php echo the_time('Y'); ?></span>
                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                <?php else : ?>
                                    <p>Search has no results.</p>
                                <?php endif; ?>
                            </div>
                            <div class="slick-dots2">
                                <div class="slick-prev2"><i class="material-icons">chevron_left</i></div>
                                <div class="dots-holder2"></div>
                                <div class="slick-next2"><i class="material-icons">chevron_right</i></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Side widget -->
                <div class="col-sm-3 col-sm-pull-9 widget-sticky">
                    <?php dynamic_sidebar('search') ?>
                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>